# oreky-replace

#### 介绍
docx文档关键词替换小工具

#### 安装教程

1. 安装依赖

```cmd
pip install -r requirements.txt
```

2. 生成exe文件
```cmd
pyi-makespec -F app.py
pyinstaller app.spec 
```

3. 将dist/app.exe与config.csv放在同一目录下, 双击app.exe即可

#### 配置文件说明

参考config.csv中的相关注释